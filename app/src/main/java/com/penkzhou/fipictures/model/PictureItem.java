package com.penkzhou.fipictures.model;

import android.database.Cursor;

import com.google.gson.Gson;
import com.penkzhou.fipictures.db.PictureDataHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by penkzhou on 14-7-8.
 *
 * id="14415969810" owner="112841662@N02"
 * secret="d791c988b5" server="2938" farm="3"
 * title="Real St.Petersburg, Russia, city center"
 * ispublic="1" isfriend="0" isfamily="0"
 * dateupload="1404801681" datetaken="2014-07-07 23:41:21"
 * datetakengranularity="0"
 * ownername="wadimf1" iconserver="0" iconfarm="0"
 * tags="square squareformat iphoneography instagramapp xproii uploaded:by=instagram"
 * media="photo" media_status="ready"
 * url_s="https://farm3.staticflickr.com/2938/14415969810_d791c988b5_m.jpg" height_s="240" width_s="240"
 * url_l="https://farm3.staticflickr.com/2938/14415969810_d480f92254_o.jpg" height_o="612" width_o="612"
 */
public class PictureItem extends BaseModel{


    private static final HashMap<Long, PictureItem> CACHE = new HashMap<Long, PictureItem>();
    private long id;
    private String owner;
    private String title;
    private String dateupload;
    private String datetaken;
    private String ownername;
    private String url_s;
    private String url_l;
    private int width_l;
    private int height_l;
    private int width_s;
    private int height_s;

    public int getWidth_l() {
        return width_l;
    }

    public void setWidth_l(int width_l) {
        this.width_l = width_l;
    }

    public int getHeight_l() {
        return height_l;
    }

    public void setHeight_l(int height_l) {
        this.height_l = height_l;
    }

    public int getWidth_s() {
        return width_s;
    }

    public void setWidth_s(int width_s) {
        this.width_s = width_s;
    }

    public int getHeight_s() {
        return height_s;
    }

    public void setHeight_s(int height_s) {
        this.height_s = height_s;
    }

    private static void addToCache(PictureItem pictureItem) {
        CACHE.put(pictureItem.getId(), pictureItem);
    }

    private static PictureItem getFromCache(long id) {
        return CACHE.get(id);
    }

    public static PictureItem fromJson(String json) {
        return new Gson().fromJson(json, PictureItem.class);
    }

    public static PictureItem fromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(PictureDataHelper.PictureDBInfo._ID));
        PictureItem pictureItem = getFromCache(id);
        if (pictureItem != null) {
            return pictureItem;
        }
        pictureItem = new Gson().fromJson(
                cursor.getString(cursor.getColumnIndex(PictureDataHelper.PictureDBInfo.JSON)),
                PictureItem.class);
        addToCache(pictureItem);
        return pictureItem;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwnerId() {
        return owner;
    }

    public void setOwnerId(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateUpload() {
        return dateupload;
    }

    public void setDateUpload(String dateupload) {
        this.dateupload = dateupload;
    }

    public String getDateTaken() {
        return datetaken;
    }

    public void setDateTaken(String datetaken) {
        this.datetaken = datetaken;
    }

    public String getOwnerName() {
        return ownername;
    }

    public void setOwnerName(String ownername) {
        this.ownername = ownername;
    }

    public String getUrl_s() {
        return url_s;
    }

    public void setUrl_s(String url_s) {
        this.url_s = url_s;
    }

    public String getUrl_l() {
        return url_l;
    }

    public void setUrl_l(String url_l) {
        this.url_l = url_l;
    }

    public static class PicturesRequestData {
        private int page;

        private int pages;

        private int perpage;

        private int total;

        private ArrayList<PictureItem> photo;

        public int getPage() {
            return page;
        }

        public int getPages() {
            return pages;
        }

        public int getPerpage() {
            return perpage;
        }

        public int getTotal() {
            return total;
        }

        public ArrayList<PictureItem> getPhoto() {
            return photo;
        }
    }
   // {"photos":{"page":1, "pages":10, "perpage":100, "total":1000, "photo"
    public static class Photos{
       private PicturesRequestData photos;
       public PicturesRequestData getPhotos(){
           return photos;
       }
       public void setPhotos(PicturesRequestData newPhotos){
           photos = newPhotos;
       }
   }
}
