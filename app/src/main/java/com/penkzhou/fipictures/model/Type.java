package com.penkzhou.fipictures.model;

/**
 * Created by penkzhou on 14-7-9.
 */
public enum Type {
    interest("flickr.interestingness.getList"), recent("flickr.photos.getRecent");
    private String mDisplayName;

    Type(String displayName) {
        mDisplayName = displayName;
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}
