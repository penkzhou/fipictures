package com.penkzhou.fipictures.model;

import com.google.gson.Gson;

/**
 * Created by penkzhou on 14-7-8.
 */
public class BaseModel {
    public String toJson(){
        return new Gson().toJson(this);
    }
}
