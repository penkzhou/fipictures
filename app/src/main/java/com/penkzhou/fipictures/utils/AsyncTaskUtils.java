package com.penkzhou.fipictures.utils;

import android.os.AsyncTask;
import android.os.Build;

/**
 * Created by penkzhou on 14-7-8.
 */
public class AsyncTaskUtils {
    public static <Params, Progress, Result> void executeAsyncTask(
            AsyncTask<Params, Progress, Result> task, Params... params) {
        if (Build.VERSION.SDK_INT >= 11) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            task.execute(params);
        }
    }
}
