package com.penkzhou.fipictures.api;

import android.util.Log;

/**
 * Created by penkzhou on 14-7-8.
 */
public class PictureAPI {

    public static final String TAG = "FlickrFetcher";

    private static final String ENDPOINT = "https://api.flickr.com/services/rest?extras=date_upload,date_taken,owner_name,url_s,url_o";
    private static final String API_KEY = "aba88cbba93fcf20386baa8cb25ce101";
    private static final String METHOD_GET_RECENT = "flickr.photos.getRecent";
    private static final String PARAM_EXTRAS = "extras";
    private static final String PARAM_EXTRAS_VALUE = "date_upload,date_taken,owner_name,url_s,url_l";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_PAGE_VALUE = "%d";
    private static final String PARAM_FORMAT = "format";
    private static final String PARAM_FORMAT_VALUE = "page";
    private static final String PARAM_NOJSONCALLBACK = "nojsoncallback";
    private static final String PARAM_NOJSONCALLBACK_VALUE = "1";
    private static final String FINAL_URL = "https://api.flickr.com/services/rest?extras=date_upload,date_taken,owner_name,url_s,url_l&method=%s&api_key=aba88cbba93fcf20386baa8cb25ce101&format=json&nojsoncallback=1&page=%d";

    private static final String XML_PHOTO = "photo";

    public static String getApiUrl(){

//        String url = Uri.parse(ENDPOINT).buildUpon()
//                .appendQueryParameter("method", METHOD_GET_RECENT)
//                .appendQueryParameter("api_key", API_KEY)
////                .appendQueryParameter(PARAM_EXTRAS, PARAM_EXTRAS_VALUE)
//                .appendQueryParameter(PARAM_FORMAT, PARAM_FORMAT_VALUE)
//                .appendQueryParameter(PARAM_NOJSONCALLBACK, PARAM_NOJSONCALLBACK_VALUE)
//                .appendQueryParameter(PARAM_PAGE, PARAM_PAGE_VALUE+"")
//                .build().toString();
        String url = FINAL_URL;
        Log.i("api",url);
        return url;
    }
}
