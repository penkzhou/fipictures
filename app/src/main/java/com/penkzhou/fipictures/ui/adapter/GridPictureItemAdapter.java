package com.penkzhou.fipictures.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.penkzhou.fipictures.R;
import com.penkzhou.fipictures.model.PictureItem;
import com.penkzhou.fipictures.network.RequestManager;

/**
 * Created by penkzhou on 14-7-8.
 */
public class GridPictureItemAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;
    private GridView mGridView;

    private Drawable mDefaultImageDrawable = new ColorDrawable(Color.argb(255, 201, 201, 201));


    public GridPictureItemAdapter(Context context, GridView gridView) {
        super(context, null, false);
        mLayoutInflater = ((Activity)context).getLayoutInflater();
        mGridView = gridView;
    }

    @Override
    public PictureItem getItem(int position) {
        mCursor.moveToPosition(position);
        return PictureItem.fromCursor(mCursor);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return mLayoutInflater.inflate(R.layout.grid_image_item, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Holder holder = getHolder(view);
        if (holder.imageRequest != null){
            holder.imageRequest.cancelRequest();
        }
        view.setEnabled(!mGridView.isItemChecked(cursor.getPosition()));
        PictureItem pictureItem = PictureItem.fromCursor(cursor);
        Log.i("adapter",pictureItem.toJson());
        holder.imageRequest = RequestManager.loadImage(pictureItem.getUrl_s(), RequestManager
                .getImageListener(holder.image, mDefaultImageDrawable, mDefaultImageDrawable));
        holder.title.setText(pictureItem.getTitle());

    }

    private Holder getHolder(final View view) {
        Holder holder = (Holder) view.getTag();
        if (holder == null) {
            holder = new Holder(view);
            view.setTag(holder);
        }
        return holder;
    }

    private class Holder {
        public ImageView image;

        public TextView title;

        public ImageLoader.ImageContainer imageRequest;

        public Holder(View view) {
            image = (ImageView) view.findViewById(R.id.iv_image);
            title = (TextView) view.findViewById(R.id.tv_title);
        }
    }
}
