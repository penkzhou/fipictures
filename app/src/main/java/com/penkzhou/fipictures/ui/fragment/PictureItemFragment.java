package com.penkzhou.fipictures.ui.fragment;



import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.penkzhou.fipictures.FIPictures;
import com.penkzhou.fipictures.R;
import com.penkzhou.fipictures.api.PictureAPI;
import com.penkzhou.fipictures.db.PictureDataHelper;
import com.penkzhou.fipictures.model.PictureItem;
import com.penkzhou.fipictures.model.Type;
import com.penkzhou.fipictures.network.GsonRequest;
import com.penkzhou.fipictures.ui.activity.MainActivity;
import com.penkzhou.fipictures.ui.activity.SinglePictureActivity;
import com.penkzhou.fipictures.ui.adapter.PictureItemAdapter;
import com.penkzhou.fipictures.ui.view.LoadingFooter;
import com.penkzhou.fipictures.utils.AsyncTaskUtils;

import java.util.ArrayList;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher;



/**
 * A simple {@link Fragment} subclass.
 *
 */
public class PictureItemFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, PullToRefreshAttacher.OnRefreshListener {
    private ListView pictureList;
    private PictureDataHelper mPictureDataHelper;
    private PictureItemAdapter pictureItemAdapter;
    private PullToRefreshAttacher mPullToRefreshAttacher;
    private LoadingFooter mLoadingFooter;
    private MainActivity mActivity;
    private Type mType;
    private int mPage = 1;
    private static final String TAG = "PictureItemFragment";
    private static final String TYPE = "TYPE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
    }

    public static PictureItemFragment newInstance(Type mType) {
        Log.i(TAG,"newInstance");
        PictureItemFragment fragment = new PictureItemFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE,mType.name());
        fragment.setArguments(bundle);
        return fragment;
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG,"onCreateView");
        View view = inflater.inflate(R.layout.fragment_pictrueitem, null);
        pictureList = (ListView)view.findViewById(R.id.pictureList);
        Log.i(TAG,"ShotsFragment.datastart");
        Bundle bundle = getArguments();
        mType = Type.valueOf(bundle.getString(TYPE));
        Log.i(TAG,"ShotsFragment.datastart " + mType);
        mPictureDataHelper = new PictureDataHelper(FIPictures.getContext(), mType);
        Log.i(TAG,"ShotsFragment.dataend");
        pictureItemAdapter = new PictureItemAdapter(getActivity(),pictureList);
        mPullToRefreshAttacher = ((MainActivity) getActivity()).getPullToRefreshAttacher();
        mPullToRefreshAttacher.setRefreshableView(pictureList, this);
        mLoadingFooter = new LoadingFooter(getActivity());
        View header = new View(getActivity());
        mLoadingFooter = new LoadingFooter(getActivity());
        pictureList.addHeaderView(header);
        pictureList.addFooterView(mLoadingFooter.getView());
        pictureList.setAdapter(pictureItemAdapter);
        getLoaderManager().initLoader(0, null, this);
        mActivity = (MainActivity) getActivity();
        pictureList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (mLoadingFooter.getState() == LoadingFooter.State.Loading
                        || mLoadingFooter.getState() == LoadingFooter.State.TheEnd) {
                    return;
                }
                if (firstVisibleItem + visibleItemCount >= totalItemCount
                        && totalItemCount != 0
                        && totalItemCount != pictureList.getHeaderViewsCount()
                        + pictureList.getFooterViewsCount() && pictureItemAdapter.getCount() > 0) {
                    loadNextPage();
                }
            }
        });

        pictureList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PictureItem chooseItem = pictureItemAdapter.getItem(position - pictureList.getHeaderViewsCount());

                Log.i(TAG, chooseItem.toJson());
                Intent intent = new Intent(getActivity(), SinglePictureActivity.class);
                intent.putExtra("Extra_json", chooseItem.toJson());
                getActivity().startActivity(intent);
            }
        });
        return view;
    }


    private void loadData(final int page) {
        Log.i(TAG,"loadData page:" + page);
        final boolean isRefreshFromTop = page == 1;
        if (!mPullToRefreshAttacher.isRefreshing() && isRefreshFromTop) {
            mPullToRefreshAttacher.setRefreshing(true);
        }
        executeRequest(new GsonRequest<PictureItem.Photos>(String.format(PictureAPI.getApiUrl(),mType.getDisplayName(),page), PictureItem.Photos.class, null,
                new Response.Listener<PictureItem.Photos>() {
                    @Override
                    public void onResponse(final PictureItem.Photos requestData) {

                        Log.i(TAG,"loadData request:" + requestData.getPhotos().getPerpage());
                        AsyncTaskUtils.executeAsyncTask(new AsyncTask<Object, Object, Object>() {
                            @Override
                            protected Object doInBackground(Object... params) {
                                PictureItem.PicturesRequestData picturesRequestData = requestData.getPhotos();
                                Log.i(TAG,"loadData request:" + picturesRequestData.getPerpage());
                                mPage = picturesRequestData.getPage();
                                if (mPage == 1) {
                                    mPictureDataHelper.deleteAll();
                                }
                                ArrayList<PictureItem> pictures = picturesRequestData.getPhoto();
                                mPictureDataHelper.bulkInsert(pictures);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Object o) {
                                super.onPostExecute(o);
                                if (isRefreshFromTop) {
                                    mPullToRefreshAttacher.setRefreshComplete();
                                } else {
                                    mLoadingFooter.setState(LoadingFooter.State.Idle, 3000);
                                }
                            }
                        });
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getActivity(), R.string.refresh_list_failed,
                        Toast.LENGTH_SHORT).show();
                if (isRefreshFromTop) {
                    mPullToRefreshAttacher.setRefreshComplete();
                } else {
                    mLoadingFooter.setState(LoadingFooter.State.Idle, 3000);
                }
            }
        }));
    }



    private void loadNextPage() {
        mLoadingFooter.setState(LoadingFooter.State.Loading);
        loadData(mPage + 1);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Log.i(TAG,"onCreateLoader");
        return mPictureDataHelper.getCursorLoader();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        Log.i(TAG,"onLoadFinished");
        pictureItemAdapter.swapCursor(data);
        Log.i(TAG,"ShotsFragment.onLoadFinished " + (data != null));
        Log.i(TAG,"ShotsFragment.onLoadFinished " + data.getCount());
        if (data != null && data.getCount() == 0) {
            loadFirstPage();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        pictureItemAdapter.swapCursor(null);
    }

    private void loadFirstPage() {
        loadData(1);
    }



    @Override
    public void onRefreshStarted(View view) {
        Log.i(TAG,"onRefreshStarted");
        loadFirstPage();
    }
}
