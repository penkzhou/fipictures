package com.penkzhou.fipictures.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.penkzhou.fipictures.R;
import com.penkzhou.fipictures.model.PictureItem;
import com.penkzhou.fipictures.network.BitmapLruCache;

import java.io.File;


/**
 * A placeholder fragment containing a simple view.
 */
public class SinglePictureFragment extends Fragment implements ImageLoader.ImageListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_JSON_STRING = "json_string";
    private static final String TAG = "SinglePictureFragment";
    private PictureItem pictureItem;
    private ImageView imageView;
    private TextView ownerText;
    private TextView dateText;
    private TextView titleText;
    // Default maximum disk usage in bytes
    private static final int DEFAULT_DISK_USAGE_BYTES = 25 * 1024 * 1024;

    // Default cache folder name
    private static final String DEFAULT_CACHE_DIR = "photos";

    // Most code copied from "Volley.newRequestQueue(..)", we only changed cache directory
    private static RequestQueue newRequestQueue(Context context) {
        // define cache folder
        File rootCache = context.getExternalCacheDir();
        if (rootCache == null) {
            Log.i(TAG, "Can't find External Cache Dir, "
                    + "switching to application specific cache directory");
            rootCache = context.getCacheDir();
        }

        File cacheDir = new File(rootCache, DEFAULT_CACHE_DIR);
        cacheDir.mkdirs();

        HttpStack stack = new HurlStack();
        Network network = new BasicNetwork(stack);
        DiskBasedCache diskBasedCache = new DiskBasedCache(cacheDir, DEFAULT_DISK_USAGE_BYTES);
        RequestQueue queue = new RequestQueue(diskBasedCache, network);
        queue.start();

        return queue;
    }

    public SinglePictureFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SinglePictureFragment newInstance(String picJson) {
        SinglePictureFragment fragment = new SinglePictureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_JSON_STRING, picJson);
        fragment.setArguments(args);
        return fragment;
    }

    public void parsePictureItem(){
        String json = getArguments().getString(ARG_JSON_STRING);
        Log.i(TAG, json);
        pictureItem = PictureItem.fromJson(json);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_single_picture, container, false);
        parsePictureItem();
        ownerText = (TextView) rootView.findViewById(R.id.tv_single_owner);
        ownerText.setText(pictureItem.getOwnerName());
        titleText = (TextView) rootView.findViewById(R.id.tv_single_title);
        titleText.setText(pictureItem.getTitle());
        getActivity().getActionBar().setTitle(pictureItem.getTitle());
        dateText = (TextView) rootView.findViewById(R.id.tv_single_date);
        dateText.setText(pictureItem.getDateTaken());
        imageView = (ImageView) rootView.findViewById(R.id.iv_single_picture);
        Log.i(TAG,"url_l : " + pictureItem.getUrl_l());
        ImageLoader.ImageCache imageCache = new BitmapLruCache();
        ImageLoader imageLoader = new ImageLoader(newRequestQueue(getActivity()), imageCache);
        imageLoader.get(pictureItem.getUrl_l(),this,pictureItem.getWidth_l(),pictureItem.getHeight_l());
        //imageView.setImageUrl(pictureItem.getUrl_l(), imageLoader);
        getActivity().setProgressBarIndeterminateVisibility(true);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }


    @Override
    public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
        if (imageContainer != null){
            getActivity().setProgressBarIndeterminateVisibility(false);
            imageView.setImageBitmap(imageContainer.getBitmap());
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {

    }
}