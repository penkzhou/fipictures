package com.penkzhou.fipictures.ui.fragment;

import android.support.v4.app.Fragment;

import com.android.volley.Request;
import com.penkzhou.fipictures.network.RequestManager;

/**
 * Created by penkzhou on 14-7-8.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onStop() {
        super.onStop();
        RequestManager.cancelAll(this);
    }

    protected void executeRequest(Request request) {
        RequestManager.addRequest(request, this);
    }
}