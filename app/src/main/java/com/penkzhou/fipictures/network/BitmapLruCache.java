package com.penkzhou.fipictures.network;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;
import com.penkzhou.fipictures.utils.ImagesUtils;

/**
 * Created by penkzhou on 14-7-8.
 */
public class BitmapLruCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {

    public BitmapLruCache(int maxSize) {
        super(maxSize);
    }

    public BitmapLruCache(){
        this(getDefaultLruCacheSize());
    }

    @Override
    protected int sizeOf(String key, Bitmap bitmap) {
        return ImagesUtils.getBitmapSize(bitmap);
    }

    public static int getDefaultLruCacheSize() {
        final int maxMemory =
                (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        return cacheSize;
    }


    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }


    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url, bitmap);
    }
}
