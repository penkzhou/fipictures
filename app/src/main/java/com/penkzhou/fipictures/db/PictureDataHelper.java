package com.penkzhou.fipictures.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.v4.content.CursorLoader;

import com.penkzhou.fipictures.model.PictureItem;
import com.penkzhou.fipictures.model.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penkzhou on 14-7-8.
 */
public class PictureDataHelper extends BaseDataHelper {
    private Type mType;

    public PictureDataHelper(Context mContext, Type mType) {
        super(mContext);
        this.mType = mType;
    }

    @Override
    public Uri getContentUri() {
        return PictureDataProvider.PICTURES_CONTENT_URI;
    }

    private ContentValues getContentValues(PictureItem pictureItem) {
        ContentValues values = new ContentValues();
        values.put(PictureDBInfo.ID, pictureItem.getId());
        values.put(PictureDBInfo.TYPE,mType.ordinal());
        values.put(PictureDBInfo.JSON, pictureItem.toJson());
        return values;
    }

    public PictureItem query(long id) {
        PictureItem pictureItem = null;
        Cursor cursor = query(null, PictureDBInfo.TYPE + "=? AND " + PictureDBInfo.ID + "= ?",
                new String[] {String.valueOf(mType.ordinal()),String.valueOf(id)
                }, null);
        if (cursor.moveToFirst()) {
            pictureItem = PictureItem.fromCursor(cursor);
        }
        cursor.close();
        return pictureItem;
    }

    public void bulkInsert(List<PictureItem> pictureItems) {
        ArrayList<ContentValues> contentValues = new ArrayList<ContentValues>();
        for (PictureItem pictureItem : pictureItems) {
            ContentValues values = getContentValues(pictureItem);
            contentValues.add(values);
        }
        ContentValues[] valueArray = new ContentValues[contentValues.size()];
        bulkInsert(contentValues.toArray(valueArray));
    }

    public int deleteAll() {
        synchronized (PictureDataProvider.DBLock) {
            PictureDataProvider.DBHelper mDBHelper = PictureDataProvider.getDBHelper();
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
//            int row = db.delete(PictureDBInfo.TABLE_NAME, PictureDBInfo.CATEGORY + "=?", new String[] {
//                    String.valueOf(mCategory.ordinal())
//            });
            int row = db.delete(PictureDBInfo.TABLE_NAME, PictureDBInfo.TYPE + "=?", new String[] {String.valueOf(mType.ordinal())});
            return row;
        }
    }

    public CursorLoader getCursorLoader() {
//        return new CursorLoader(getContext(), getContentUri(), null, PictureDBInfo.CATEGORY + "=?",
//                new String[] {
//                        String.valueOf(mCategory.ordinal())
//                }, PictureDBInfo._ID + " ASC");
        return new CursorLoader(getContext(), getContentUri(), null,  PictureDBInfo.TYPE + "=?", new String[] {String.valueOf(mType.ordinal())}, PictureDBInfo._ID + " ASC");
    }




    public static final class PictureDBInfo implements BaseColumns {
        private PictureDBInfo() {
        }

        public static final String TABLE_NAME = "pictures";

        public static final String ID = "id";

        public static final String TYPE = "type";

        public static final String JSON = "json";

        public static final SQLiteTable TABLE = new SQLiteTable(TABLE_NAME)
                .addColumn(ID, Column.DataType.INTEGER)
                .addColumn(TYPE, Column.DataType.INTEGER)
                .addColumn(JSON, Column.DataType.TEXT);
    }

}
